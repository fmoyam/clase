package com.sjava.testmoto;

class Moto {
    String marca;
    String modelo;
    int cc;
    int cv;
    int pvp;

    public Moto(String marca, String modelo, int cc, int cv, int pvp) {
        this.marca = marca;
        this.modelo = modelo;
        this.cc = cc;
        this.cv = cv;
        this.pvp = pvp;
    }

    public void comparaCv(Moto otra){
        if (this.cv > otra.cv) {
            System.out.printf(
                "La %s %s es más potente que la %s %s",
                this.marca, this.modelo, otra.marca, otra.modelo);
        } else {
            System.out.printf(
                "La %s %s es más potente que la %s %s",
                otra.marca, otra.modelo, this.marca, this.modelo);   
        }
    }

    public void compPrecio(Moto otra){
		if(this.pvp > otra.pvp){
            System.out.printf("La %s %s %d es mas cara que la %s %s %d",
            this.marca, this.modelo, this.pvp, otra.marca, otra.modelo, otra.pvp);
        }else{
            System.out.printf("La %s %s %d es mas cara que la %s %s %d",
            otra.marca, otra.modelo, otra.pvp, this.marca, this.modelo, this.pvp);
        }
    }
}


