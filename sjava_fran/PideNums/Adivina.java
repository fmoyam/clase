//-----------EJ Adivina-----------
import java.util.Scanner;
import java.util.Random;

class Adivina{
public static void main(String[] args) {
    
        Scanner keyboard = new Scanner(System.in);
        Random random = new Random();
        int incognita = random.nextInt(10)+1;
        System.out.println(incognita);
        int num=0;
        do{
            try {
                System.out.println("Introduce un num: ");
                num = keyboard.nextInt();
            }catch(Exception e){
                System.out.println("ERROR prueba otra combinacion");
                keyboard.next();
                num = keyboard.nextInt();            
                }
        }while(num!=incognita);
        System.out.println("ENHORABUENA acertaste el numero");
        keyboard.close();
    }
}