
import java.util.Scanner;
/**
 * Juego tic-tac-toe
 * 
 * Método principal "play"
 * Curso Java Salamanca Mayo/Junio 2018
 * Mini-Proyecto
 * 
 * @author XXX
 * @version 1.0
 * 
 */
class Tictactoe {
    
    //constantes utilizadas en el código
    final String[] SIMBOLOS = new String[] {"-","X","O"};
    final int GANA1 = 1;
    final int GANA2 = 2;
    final int EMPATE = 0;
    final int SEGUIR = -1;
    // array 2D (matriz) de posiciones ganadoras
    final int[][] WINNERS = { 
        {1,2,3},
        {4,5,6},
        {7,8,9},
        {1,4,7},
        {2,5,8},
        {3,6,9},
        {1,5,9},
        {7,5,3}
    };

    //mapa de posiciones ocupadas, un array de 9 enteros indicando:
    //  (0) posición libre (1) jugador1 (2) jugador2
    int[] map = new int[] {0,0,0,0,0,0,0,0,0};
    // inicializamos keyboard como atributo de clase para poderlo utilizar en varios métodos
    Scanner keyboard = new Scanner(System.in);
    
    

    /**
     * Método principal
     * Crea un bucle indefinido en que ejecuta los dos turnos, uno para cada jugador
     * Siempre empieza el jugador 1
     * En cada turno se llama al método "turno" que pide la jugada y redibuja el mapa
     * 
     */

    public void play(){
        
        int respuesta;

        //dibujamos tablero por primera vez
        draw();

        do {
            // invocamos turno con número de jugador y recibimos respuesta
            respuesta = turno(1);
            // si respuesta es distinta de SEGUIR, saltamos el turno 2
            if (respuesta!=SEGUIR) continue;
            // turno 2, ejecutamos y esperamos respuesta 
            respuesta = turno(2);
            
        } while (respuesta == SEGUIR);
        // hemos salido del bucle, por tanto tenemos o ganador o empate
        // mostramos mensaje adecuado
        switch(respuesta) {
            case GANA1:
                System.out.println("Gana el jugador 1");
                break;
            case GANA2:
                System.out.println("Gana el jugador 2");
                break;
            default:
                System.out.println("Empate, no hay más posiciones");
                break;
        }
        
        //cerramos teclado, aunque no es imprescindible
        keyboard.close();
    }

    /**
     * Método que ejecuta la jugada:
     *  pide posición al jugador y devuelve resultado
     *  A COMPLETAR: debería verificar si la posición está ocupada, y en este caso volverla a pedir
     */
    int turno(int jugador){
        int resp;
        int posicion=0;
        //mostramos pregunta y esperamos número introducido
        do{
            System.out.printf("Jugador %d: ", jugador);
        try{
            posicion = keyboard.nextInt();

        }catch(Exception e){       //evaluamos que no nos meta un letra
            draw();
            System.out.println("El caracter introducido no es valido rango: 1-9");
        }
        //se evalua la condicion por si es un numero y no esta entre 1-9
        if(posicion<1 || posicion>9){
            draw();
            System.out.println("El rango tiene que ser de 1-9");
            keyboard.next();
        }
        }while(posicion<1 || posicion>9);
        
        //establecemos posición en el mapa para jugador actual
        setMap(posicion, jugador);
        //mostramos mapa actualizado
        draw();
        
        //calcula respuesta que debe retornar
        if (winner(jugador)) {
            resp = (jugador==1) ? GANA1 : GANA2;
        } else if (numZeros()==0) {
            resp = EMPATE;
        } else {
            resp = SEGUIR;
        }

        return resp;
    }

    /**
     * método que verifica si en las posiciones actuales del tablero el jugador 
     * recibido es el ganador, y en este caso devolver true, de lo contrario false
     * A COMPLETAR: debe verificar realmente las jugadas!
     * pista: utilizar el array WINNERS...
     */
    boolean winner(int jugador) {
        
        for ( int [] trio : WINNERS) {
            if(getMap(trio[0])==jugador && getMap(trio[1])==jugador && getMap(trio[2])==jugador){
                return true;
            }   
        }
        return false;
    }

    // devuelve el número de posiciones 0 que hay en el tablero
    // si no queda ninguna, significará que se ha terminado la partida (devuelve 0)
    int numZeros(){
        int zeros=0;
        for(int i : map) {
            if (i==0) zeros++;
        }
        System.out.println("Se han rellenado todas las casillas");
        return zeros;
    }

    // establece la posición del mapa al valor recibido (1/2 segun jugador)
    // IMPORTANTE restamos 1 a la posición puesto que las posiciones van de 1 a 9 y el array de 0 a 8!
    void setMap(int posicion, int valor){
		this.map[posicion-1]=valor;
	}
    
    // devuelve el valor que hay en la posición recibida. 
    // restamos también una unidad a la posición!
	int getMap(int posicion){
		return this.map[posicion-1];
	}
	
    // muesta mapa en pantalla, MEJORABLE
	public void draw() {
        String l1 = this.SIMBOLOS[getMap(7)] + "  " + this.SIMBOLOS[getMap(8)] + "  " + this.SIMBOLOS[getMap(9)];
        String l2 = this.SIMBOLOS[getMap(4)] + "  " + this.SIMBOLOS[getMap(5)] + "  " + this.SIMBOLOS[getMap(6)];
        String l3 = this.SIMBOLOS[getMap(1)] + "  " + this.SIMBOLOS[getMap(2)] + "  " + this.SIMBOLOS[getMap(3)];
        System.out.println();
        System.out.println();
        System.out.println("3enRAYA");
        System.out.println("-------");
        System.out.println(l1);
        System.out.println(l2);
        System.out.println(l3);
        System.out.println("-------");
        
    }
    
}

