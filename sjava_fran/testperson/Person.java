import java.beans.PersistenceDelegate;

class Person {
    String nombre;
    int edad;
    
    Person(String n,int e){
        this.nombre = n;
        this.edad = e;
    }

    void presentacion(){
        System.out.println("Hola "+nombre+" su edad es: "+edad);
    }

    public void compararEdad(int a){
        if(this.edad>a){
            System.out.println("La primera persona "+this.edad+" es mayor que la segunda persona"+a);
        }else{
            System.out.println("La segunda persona "+a+" es mayor que la primera persona "+this.edad);
        }
    }

}
